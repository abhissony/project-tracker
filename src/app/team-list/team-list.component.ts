import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
declare const $: any;

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {
teams: any[]=[];
HtmlForAddProject ='';
popUpButtonText= '';
popUpTitleText = '';
  constructor() { }

  ngOnInit() {
  	  	this.teams.push(
  	  {teamId:1, teamName :'MEAN STACK', teamManager:'@Singh', teamDesc: 'on Home PAGE', teamLeads: [{teamLeadId : 11, teamLeadName :'kunvar'},{teamLeadId : 21, teamLeadName :'singh'},{teamLeadId : 31, teamLeadName :'kunvar singh'}]},
  	  {teamId:2, teamName :'MEAN STACK', teamManager:'@Singh', teamDesc: 'on Home PAGE', teamLeads: [{teamLeadId : 12, teamLeadName :'kunvar'}]},
  	  {teamId:3, teamName :'MEAN STACK', teamManager:'@Singh', teamDesc: 'on Home PAGE', teamLeads: [{teamLeadId : 13, teamLeadName :'kunvar'},{teamLeadId : 32, teamLeadName :'@singh'}]},
  	  {teamId:4, teamName :'MEAN STACK', teamManager:'@Singh', teamDesc: 'on Home PAGE', teamLeads: [{teamLeadId : 14, teamLeadName :'kunvar'}]},
  	);

  	this.HtmlForAddProject =`
    <form [formGroup]="teamForm" (ngSubmit)='saveTeam()'>
     <div class="card-content">
                          <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text"  formControlName ='teamName' placeholder="Team Name" class="form-control" />
                            </div>
                        </div>
                           
                        <div class="col-sm-12">
                            <div class="form-group">
                            <textarea class="form-control" formControlName ='teamDesc'  placeholder="Team Description" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="dropdown">
								<a href="#" class="btn dropdown-toggle" data-toggle="dropdown">
							    	Select Manager
							    	<b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
									<li class="divider"></li>
									<li><a href="#">One more separated link</a></li>
								</ul>
							</div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                         <div class="form-group">
                                <div class="dropdown">
								<a href="#" class="btn dropdown-toggle" data-toggle="dropdown">
							    	Select Leads
							    	<b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
									<li class="divider"></li>
									<li><a href="#">One more separated link</a></li>
								</ul>
							</div>
                        </div>
                       </div>
                        </div>
                      </form>  `;

  }

  addTeam () :void  {
    this.popUpButtonText ='Save';
    this.popUpTitleText ='Add Team';
    this.popUpForAddOrEdit();
  }

    popUpForAddOrEdit() :void {

    swal({
        title: this.popUpTitleText,
        type: 'info',
        html:this.HtmlForAddProject,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        cancelButtonColor: '#d33',
        confirmButtonText:
          '<i class="fa fa-save"></i>'+this.popUpButtonText,
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText:
        '<i class="fa fa-delete"></i> Cancel',
        cancelButtonAriaLabel: 'Thumbs down',
      }).then(function (result) {
        if (result.value) {
          this.saveProject();
        }
        else{

        }
      })
      
  }

  editTeam(item: any) :void {
  	this.popUpButtonText ='Update';
    this.popUpTitleText ='Update Team';
    this.popUpForAddOrEdit();
  }

  deleteTeam(item :any) :void {
  	 swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(function (result) {
        if (result.value) {
          for(let i=0; i<this.projects.length ; i++){
            debugger;
            if(this.projects[i].projectId == item.projectId){
             this.projects.splice(this.projects[i], 1);
            }
          }
          // this.projects.splice(item,1);
          swal(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
        }
        else{
          // press cancel delete
        }
      })
  }

}
