import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from  '@angular/router';
import {GlobalService} from '../GlobalService';
import swal from 'sweetalert2';
declare const $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loader: boolean= false;
    is_login_email: boolean;
    public user;
    sub: any;
    subscription:boolean=true;
    display:boolean = false;
    forgotPasswordForm: FormGroup;
    disableMultipleSave:boolean = false;
    validateFields:boolean = false;
    HtmlForAddProject = '';

  constructor(public fb: FormBuilder, public global_service: GlobalService, public router: Router) {
    this.HtmlForAddProject =` <div class="card-content">
                          <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" value="" placeholder="Old Password" class="form-control" />
                            </div>
                        </div>
                           
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" value="" placeholder="New Password" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" value="" placeholder="Confirm Password" class="form-control" />
                            </div>
                        </div>
                        </div>
                        `;
   }

  openRegister () :void{

    $('.toggle').on('click', function() {
      $('.container').stop().addClass('active');
    });

    $('.close').on('click', function() {
      $('.container').stop().removeClass('active');
    });
    
   }

  ngOnInit() {
     this.formInitialization();
    }

    formInitialization(){
     this.loginForm = this. fb.group({
      username: ['singh@yopmail.com',[Validators.required]],
      password: ['singh', [Validators.required]]
     })
    }

    login(){

     this.loader = true;
      
      const url = this.global_service.basePath + 'api/login';
      // this.loginForm.value.password = window.btoa(this.loginForm.value.password); // for encrypt/decrypt password
      this.global_service.PostRequestUnautorized(url , this.loginForm.value)
        .subscribe(res => {
          this.loader = false;
          this.loginForm.reset();
          /*Save Logged in user Data in the Localstorage for later use*/
          if(res[0].json.data){
            localStorage.setItem('userInfo', JSON.stringify(res[0].json.data));
            localStorage.setItem('token', res[0].json.token)
          }

          if (res[0].json.status == 200){ 
             this.global_service.showNotification('top','right','Login Successfully',2,'ti-check');
          }
          else {
             this.global_service.showNotification('top','right',res[0].json.error.msg,4,'ti-cross');   
          }

          /*for Global setting of user role*/
          this.global_service.loggedInVar = true;
          this.global_service.loggedInObs.next(this.global_service.loggedInVar);
          
          /*Redirect on the View-User*/
          this.router.navigateByUrl('/dashboard');

        },
        err => {
          this.loader = false;
          this.global_service.showNotification('top','right',err.json().msg,4,'ti-cross');   
          this.loginForm.reset();
        })
    }

    forgotPassword() : void{
      swal({
        title: 'Forgot Password',
        type: 'info',
        html:this.HtmlForAddProject,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        cancelButtonColor: '#d33',
        confirmButtonText:
          '<i class="fa fa-save"></i> Save',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText:
        '<i class="fa fa-delete"></i> Cancel',
        cancelButtonAriaLabel: 'Thumbs down',
      }).then(function (result) {
        if (result.value) {
          this.saveProject();
        }
        else{

        }
      })
    }


}
