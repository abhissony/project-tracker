import { Component, OnInit } from '@angular/core';
import { GlobalService} from '../GlobalService';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import swal from 'sweetalert2';
import * as moment from 'moment/moment';
declare const $: any;

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {
tasks : any[] = [];
popUpButtonText ='';
popUpTitleText = '';
HtmlForAddProject ='';
isDataFound :boolean =false;
loader :boolean = false;
usersCount : Number=0;
tasksCount : Number=0;
users : any[] =[];
  constructor(private global_service : GlobalService, public fb: FormBuilder) { 
    this.getTasksList(1);
    this.getUsersList();
  }

  ngOnInit() {
  	// this.tasks.push(
  	//   {taskId:1, taskName :'please! Oops resove me as soon as', assignedBy:'@Singh', taskDesc: 'on Home PAGE', startDate: new Date(), endDate: new Date()},
  	//   {taskId:1, taskName :'please! Oops resove me as soon as', assignedBy:'@Singh', taskDesc: 'on Home PAGE', startDate: new Date(), endDate: new Date()},
  	//   {taskId:1, taskName :'please! Oops resove me as soon as', assignedBy:'@Singh', taskDesc: 'on Home PAGE', startDate: new Date(), endDate: new Date()},
  	//   {taskId:1, taskName :'please! Oops resove me as soon as', assignedBy:'@Singh', taskDesc: 'on Home PAGE', startDate: new Date(), endDate: new Date()},
  	// );

  	this.HtmlForAddProject =`
    <form [formGroup]="teamForm" (ngSubmit)='saveTeam()'>
     <div class="card-content">
                          <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text"  formControlName ='teamName' placeholder="Team Name" class="form-control" />
                            </div>
                        </div>
                           
                        <div class="col-sm-12">
                            <div class="form-group">
                            <textarea class="form-control" formControlName ='teamDesc'  placeholder="Team Description" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="dropdown">
								<a href="#" class="btn dropdown-toggle" data-toggle="dropdown">
							    	Select Manager
							    	<b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li *ngFor="let manager of users"><a href="#">{{manager.userName}}</a></li>
								</ul>
							</div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                         <div class="form-group">
                                <div class="dropdown">
								<a href="#" class="btn dropdown-toggle" data-toggle="dropdown">
							    	Select Leads
							    	<b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
									<li class="divider"></li>
									<li><a href="#">One more separated link</a></li>
								</ul>
							</div>
                        </div>
                       </div>
                        </div>
                      </form>  `;

  }

  getTasksList(pageNumber? :any) {
    this.loader = true;
    const url = this.global_service.basePath+'users/viewTasks';
    this.tasks=[];
    this.global_service.GetRequest(url).subscribe(res => {
     if(res[0])
     {
       this.isDataFound =true;
      for(let result of res[0].json.result){
        this.tasksCount = res[0].json.totalRecords;
        this.tasks.push(
          {
            taskId : result._id,
            taskName: result.taskTitle,
            taskDesc : result.taskDetails,
            // startDate : moment(new Date(result.projectStartDate)).format('D MMM YYYY'),
            // endDate : moment(new Date(result.projectEndDate)).format('D MMM YYYY'),
            assignedBy : result.assignBy
          });
      }
      this.loader = false;
     }
    }, err => {
      this.isDataFound =false;
      this.loader = false;
      this.global_service.consoleFun(err);
    })
  }

  getUsersList() {
    this.loader = true;
    const url = this.global_service.basePath+'users/getAllUsers';
    this.tasks=[];
    this.global_service.GetRequest(url).subscribe(res => {
     if(res[0])
     {
      this.isDataFound =true;
      for(let result of res[0].json.result){
        this.usersCount = res[0].json.totalRecords;
        this.users.push(
          {
            userId : result._id,
            userName: result.firstName+" "+result.lastName,
            accountType : result.accountType
          });
      }
      debugger;
      this.loader = false;
     }
    }, err => {
      this.isDataFound =false;
      this.loader = false;
      this.global_service.consoleFun(err);
    })
  }


  addTask () :void  {
    this.popUpButtonText ='Save';
    this.popUpTitleText ='Add Task';
    this.popUpForAddOrEdit();
  }

    popUpForAddOrEdit() :void {

    swal({
        title: this.popUpTitleText,
        type: 'info',
        input: 'email',
        inputValue: "E.g john",
        html:this.HtmlForAddProject,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        cancelButtonColor: '#d33',
        confirmButtonText:
          '<i class="fa fa-save"></i>'+this.popUpButtonText,
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText:
        '<i class="fa fa-delete"></i> Cancel',
        cancelButtonAriaLabel: 'Thumbs down',
      }).then(function (result) {
        if (result.value) {
          var dtaa= swal.getInput();
          debugger;
          this.saveProject();
        }
        else{

        }
      })
      
  }

  editTask(item: any) :void {
  	this.popUpButtonText ='Update';
    this.popUpTitleText ='Update Team';
    this.popUpForAddOrEdit();
  }

  deleteTask(item :any) :void {
  	 swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
       
        let data ={
           uid : item.teamId
         }
          const url = this.global_service.basePath+'users/deleteTask';
    
          this.global_service.DeleteRequest(url ,data).subscribe(res => {
            debugger;
           if(res[0]){
             this.global_service.showNotification('top','right',JSON.parse(res[0].json._body).err,2,'ti-check');
             this.getTasksList(1);
           }
          }, err => {
            this.loader = false;
            this.global_service.consoleFun(err);
          });

      })
  }

}
