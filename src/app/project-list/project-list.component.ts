import { Component, OnInit } from '@angular/core';
import { GlobalService} from '../GlobalService';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import swal from 'sweetalert2';
import * as moment from 'moment/moment';
declare const $: any;


@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})


export class ProjectListComponent implements OnInit {

private projectsList : any[] =[];
isDataFound :boolean =false;
loader :boolean = false;
viewProject : boolean = true;
projects : any[] = [];
HtmlForAddProject = '';
popUpTitle ='';
popUpButtonSave ='';
projectForm : FormGroup;
name ='';
teams : any[]= [];
projectCount : Number =0;

  constructor(private global_service : GlobalService, public fb: FormBuilder) {

    // this.projects.push(
    //   {projectId : '1' , projectName : 'adfsfdsd', projectDesc : 'Hey I am New Project', startDate : moment(new Date()).format('D MMM YYYY'), endDate : moment().format('D MMM YYYY')},
    //   {projectId : '2' , projectName : 'fsdfsdfd', projectDesc : 'Hey I am New Project', startDate : new Date(), endDate : new Date()},
    //   {projectId : '3' , projectName : 'sfhfghfgh', projectDesc : 'Hey I am New Project', startDate : new Date(), endDate : new Date()},
    //   {projectId : '4' , projectName : 'gjghkghmnb', projectDesc : 'Hey I am New Project', startDate : new Date(), endDate : new Date()},
    //   {projectId : '5' , projectName : 'mhjbmbnm', projectDesc : 'Hey I am New Project', startDate : new Date(), endDate : new Date()},
    //   {projectId : '6' , projectName : 'bnmjhkyuiy', projectDesc : 'Hey I am New Project', startDate : new Date(), endDate : new Date()}
    //   );

    this.HtmlForAddProject =`
    <form [formGroup]="projectForm" (ngSubmit)='saveProject()'>
     <div class="card-content">
                          <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" id="pName" formControlName ='projectName' placeholder="Project Name" class="form-control" />
                            </div>
                        </div>
                           
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea class="form-control" id="pDesc" formControlName ='endDate'  placeholder="Project Description" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text"  id="pStartdate" value="" formControlName ='startDate' placeholder="End Date" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                         <div class="form-group">

                          <p-calendar type="date" formControlName="birthdate"  placeholder="Enter the End Date" [(ngModel)]="needbydate" 
                            [monthNavigator]="true" [yearNavigator]="true" yearRange="0:2020">
                          </p-calendar>

                         <input type="text"  id="pEndDate" value="" formControlName ='endDate' placeholder="End Date" class="form-control" />
                        </div>
                       </div>
                        </div>
                      </form>  `;
   }

  ngOnInit() {
  	this.getProjectList(1);
    this.loader = true;
  }

  formInitialization(){
     this.projectForm = this. fb.group({
      projectName: ['',[Validators.required]],
      projectDesc: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]]
     })
    }

    getProjectList(pageNumber? :any) {
    this.loader = true;
    const url = this.global_service.basePath+'users/viewprojects/'+pageNumber;
    this.projects=[];
    this.global_service.GetRequest(url).subscribe(res => {
     if(res[0])
     {
       this.isDataFound =true;
      for(let result of res[0].json.result){
        this.projectCount = res[0].json.totalRecords;
        this.projects.push(
          {
            projectId: result._id,
            projectName : result.projectName,
            projectStartDate : moment(new Date(result.projectStartDate)).format('D MMM YYYY'),
            projectEndDate : moment(new Date(result.projectEndDate)).format('D MMM YYYY'),
            projectDesc : result.projectDetails
          });
      }
      this.loader = false;
     }
    }, err => {
      this.isDataFound =false;
      this.loader = false;
      this.global_service.consoleFun(err);
    })
  }

paginate(event) {
        this.getProjectList(event.page+1);
}

  addProject () :void  {
  	this.viewProject = false;
    this.popUpButtonSave ='Save';
    this.popUpTitle ='Add Project';
    this.popUpForAddOrEdit();
    this.formInitialization();
  }

   editProject (item : any) :void {
     this.popUpButtonSave ='Update';
    this.popUpTitle ='Edit Project';
    this.popUpForAddOrEdit();
  }

  saveProject () : void {
    let apiURL = (this.popUpButtonSave === 'Save' ? 'api/addproject' : 'api/updateproject');
  	this.viewProject = true;

    let data = {
              projectName : $('#pName').val(),
              projectDetails : $('#pDesc').val(),
              projectStartDate : $('#pStartdate').val(),
              projectEndDate : $('#pEndDate').val(),
            };
    const url = this.global_service.basePath+'users/createproject';
    
    this.global_service.PostRequest(url ,data).subscribe(res => {
      debugger;
     if(res[0]){
       this.global_service.showNotification('top','right',JSON.parse(res[0].json._body).msg,2,'ti-check');
       this.getProjectList(1);
     }
    }, err => {
      this.loader = false;
      this.global_service.consoleFun(err);
    });
  }

  popUpForAddOrEdit() :void {

    swal({
        title: this.popUpTitle,
        type: 'info',
        html:this.HtmlForAddProject,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        cancelButtonColor: '#d33',
        confirmButtonText:
          '<i class="fa fa-save"></i>'+this.popUpButtonSave,
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText:
        '<i class="fa fa-delete"></i> Cancel',
        cancelButtonAriaLabel: 'Thumbs down',
      }).then((result) => {
              this.saveProject();
      })
      
      if(this.popUpButtonSave == 'Update') {
         $('#pName').val('kunvar');
        setTimeout(function(){
          this.name='kunvar';
        this.projectForm.controls['projectName'].setValue('kunvar');
         },1000);
       }

       if(this.popUpButtonSave == 'Save') {

       }
  }

  deleteProject (item : any) : void{
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
         
         let data ={
           uid : item.projectId
         }
          const url = this.global_service.basePath+'users/deleteproject';
    
          this.global_service.DeleteRequest(url ,data).subscribe(res => {
            debugger;
           if(res[0]){
             this.global_service.showNotification('top','right',JSON.parse(res[0].json._body).msg,2,'ti-check');
             this.getProjectList(1);
           }
          }, err => {
            this.loader = false;
            this.global_service.consoleFun(err);
          });

          // this.projects.splice(item,1);
          // swal(
          //   'Deleted!',
          //   'Your file has been deleted.',
          //   'success'
          // )
      })
  }

  getAllTeam () : void {
    this.teams.push(
      {teamId : 1, teamName : 'Mean Stack'},
      {teamId : 1, teamName : 'UI Developer'},
      {teamId : 1, teamName : 'Java'},
      {teamId : 1, teamName : ' .Net'}
      );
  }

  assignProject (item :any) :void{
    this.getAllTeam();
    
    swal({
        title: 'Assign Project',
        type: 'info',
        html: `
              <div class="col-sm-6">
                    <div class="form-group">
                        <div class="dropdown">
                        <a href="#" class="btn dropdown-toggle" data-toggle="dropdown">
                            Select Team
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                          <li><a href="#">Action</a></li>
                          <li><a href="#">Another action</a></li>
                          <li><a href="#">Something else here</a></li>
                          <li class="divider"></li>
                          <li><a href="#">Separated link</a></li>
                          <li class="divider"></li>
                          <li><a href="#">One more separated link</a></li>
                        </ul>
                      </div>
                    </div>
              </div>
        `,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        cancelButtonColor: '#d33',
        confirmButtonText:
          '<i class="fa fa-save"></i> Assign',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText:
        '<i class="fa fa-delete"></i> Cancel',
        cancelButtonAriaLabel: 'Thumbs down',
      }).then(function (result) {
        if (result.value) {

        }
        else{

        }
      })
  }
 


}
