import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';

import { GlobalService} from './GlobalService'; 
import { ToasterModule, ToasterContainerComponent, ToasterService, ToasterConfig} from 'angular2-toaster';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder } from '@angular/forms';
import { TaskListComponent } from './task-list/task-list.component';
import { TeamListComponent } from './team-list/team-list.component';
import { ProjectService} from './project-list/shared/project.service';
import { TaskService} from './task-list/shared/task.service';
import { TeamService} from './team-list/shared/team.service';
import { PaginatorModule, CalendarModule, ListboxModule, MultiSelectModule, TabViewModule, AccordionModule, MenuItem, SliderModule, DialogModule, ConfirmDialogModule} from 'primeng/primeng';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    UserProfileComponent,
    ProjectListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    LoginComponent,
    SignupComponent,
    TaskListComponent,
    TeamListComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    ReactiveFormsModule
    // ,PaginatorModule, CalendarModule
    //, PaginatorModule, CalendarModule, ListboxModule, MultiSelectModule, TabViewModule, AccordionModule, SliderModule, DialogModule, ConfirmDialogModule
  ],
  providers: [GlobalService , ToasterService, ProjectService, TeamService, TaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
